-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 25 mai 2022 à 12:09
-- Version du serveur : 10.4.24-MariaDB
-- Version de PHP : 8.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE if NOT EXISTS base_de_donnees_php;
USE base_de_donnees_php;
DROP TABLE IF EXISTS order_product;
DROP TABLE IF EXISTS order_product;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS products;
DROP TABLE IF EXISTS customers;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `base_de_donnees_php`


-- Structure de la table `customers`
--

CREATE TABLE `customers` (
                             `id` smallint(5) UNSIGNED NOT NULL,
                             `pseudo` varchar(20) NOT NULL,

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `customers`
--

INSERT INTO `customers` (`id`, pseudo)VALUES
                             (1, 'user1'),
                             (2, 'user2');


-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

CREATE TABLE `orders` (
                          `id` smallint(5) UNSIGNED NOT NULL,
                          `number` int(10) UNSIGNED NOT NULL,
                          `customer_id` smallint(5) UNSIGNED NOT NULL,
                          `total` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `orders`
--

INSERT INTO `orders` (`id`, `number`, `customer_id`, `total`) VALUES
                                                                          (1, 2220510, 2, 12000),
                                                                          (2, 2220519, 2, 15000),
                                                                          (3, 1220517, 1, 2000),
                                                                          (4, 1220511, 1, 5000),
                                                                          (5, 1220519, 1, 9000);

-- --------------------------------------------------------

--
-- Structure de la table `order_product`
--

CREATE TABLE `order_product` (
                                 `id` smallint(5) UNSIGNED NOT NULL,
                                 `order_id` smallint(5) UNSIGNED NOT NULL,
                                 `product_id` smallint(5) UNSIGNED NOT NULL,
                                 `quantity` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `quantity`) VALUES
                                                                             (1, 1, 1, 1),
                                                                             (2, 1, 3, 2),
                                                                             (3, 2, 13, 1),
                                                                             (4, 2, 10, 2),
                                                                             (5, 3, 2, 1),
                                                                             (6, 3, 9, 1),
                                                                             (7, 4, 4, 2),
                                                                             (8, 4, 12, 1),
                                                                             (9, 5, 2, 1),
                                                                             (10, 5, 11, 1);

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE `products` (
                            `id` smallint(5) UNSIGNED NOT NULL,
                            `name` varchar(20) NOT NULL,
                            `image_url` varchar(255) NOT NULL,
                            `description` text NOT NULL,
                            `weight` int(10) UNSIGNED NOT NULL,
                            `available` tinyint(4) DEFAULT 1,
                            `price` int(10) UNSIGNED NOT NULL,
                            `quantity` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `name`, `image_url`, `description`, `weight`, `available`, `price`, `quantity`) VALUES
                                                                                                                                  (1, 'Sed a lorem', 'https://picsum.photos/536/354', 'Suspendisse ac posuere felis, varius malesuada erat. Integer condimentum venenatis pellentesque. Praesent id interdum tortor, non porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 1000, 1, 10000, 10, 3),
                                                                                                                                  (2, 'Smalesuada', 'https://picsum.photos/537/354', 'Pon porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 1000, 1, 10000, 10, 1),
                                                                                                                                  (3, 'Varius', 'https://picsum.photos/536/355', 'Sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 500, 1, 1000, 1, 1),
                                                                                                                                  (4, 'Pellentesque', 'https://picsum.photos/537/355', 'Porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 500, 1, 1000, 0, 1),
                                                                                                                                  (5, 'Praesent', 'https://picsum.photos/536/353', 'Venenatis pellentesque. Praesent id interdum tortor, non porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 500, 0, 1000, 1, 1),
                                                                                                                                  (6, 'Interdum', 'https://picsum.photos/537/353', 'Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 500, 0, 1000, 1, 1),
                                                                                                                                  (7, 'Velit', 'https://picsum.photos/538/354', 'Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 500, 1, 1300, 2, 2),
                                                                                                                                  (8, 'Ultrices', 'https://picsum.photos/538/355', 'Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus. Praesent id interdum tortor, non porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 500, 1, 1300, 2, 2),
                                                                                                                                  (9, 'Accumsan', 'https://picsum.photos/533/354', 'Praesent id interdum tortor, non porttitor velit. Suspendisse ac posuere felis, varius malesuada erat. Integer condimentum venenatis pellentesque. Praesent id interdum tortor, non porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 1200, 1, 5000, 2, 2),
                                                                                                                                  (10, 'Felis', 'https://picsum.photos/533/355', 'Integer condimentum venenatis pellentesque. Praesent id interdum tortor, non porttitor velit. Suspendisse ac posuere felis, varius malesuada erat. Integer condimentum venenatis pellentesque. Praesent id interdum tortor, non porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 1200, 1, 5000, 2, 2),
                                                                                                                                  (11, 'Nibh', 'https://picsum.photos/532/355', 'Sed a lorem feugiat, interdum est vitae, tempus risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer condimentum venenatis pellentesque. Praesent id interdum tortor, non porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 1200, 1, 5000, 5, 3),
                                                                                                                                  (12, 'Porttitor', 'https://picsum.photos/532/354', 'Interdum et malesuada fames ac ante ipsum primis in faucibus.Integer condimentum venenatis pellentesque. Praesent id interdum tortor, non porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 1200, 1, 5000, 5, 3),
                                                                                                                                  (13, 'Pellentesque', 'https://picsum.photos/532/356', 'Fames ac ante ipsum primis in faucibus. Integer condimentum venenatis pellentesque. Praesent id interdum tortor, non porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 1200, 1, 5000, 5, 3);

--
-- Index pour les tables déchargées
--

--
--
-- Index pour la table `customers`
--
ALTER TABLE `customers`
    ADD PRIMARY KEY (`id`);

--
-- Index pour la table `orders`
--
ALTER TABLE `orders`
    ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Index pour la table `order_product`
--
ALTER TABLE `order_product`
    ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Index pour la table `products`
--
ALTER TABLE `products`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--
--
-- AUTO_INCREMENT pour la table `customers`
--
ALTER TABLE `customers`
    MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `orders`
--
ALTER TABLE `orders`
    MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `order_product`
--
ALTER TABLE `order_product`
    MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
    MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `orders`
--
ALTER TABLE `orders`
    ADD CONSTRAINT `customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `order_product`
--
ALTER TABLE `order_product`
    ADD CONSTRAINT `order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;



COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
