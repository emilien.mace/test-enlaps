CREATE DATABASE if NOT EXISTS laravel;
USE laravel;
DROP TABLE IF EXISTS order_product;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS products;
DROP TABLE IF EXISTS users;

CREATE TABLE `users` (
                         id INTEGER not null primary key autoincrement,
                             `pseudo` varchar(20) NOT NULL

);

INSERT INTO `users` (`id`, pseudo)VALUES
                                          (1, 'user1'),
                                          (2, 'user2');
CREATE TABLE `orders` (
                          id INTEGER not null primary key autoincrement,
                          `number` int(10)  NOT NULL,
                          `user_id` smallint(5)  NOT NULL,
                          `total` int(10)  NOT NULL
) ;

INSERT INTO `orders` (`number`, `user_id`, `total`) VALUES
                                                                  (2220510, 2, 12000),
                                                                  (2220519, 2, 15000),
                                                                  (1220517, 1, 2000),
                                                                  (1220511, 1, 5000),
                                                                  (1220519, 1, 9000);



CREATE TABLE `order_product` (
                                 id INTEGER not null primary key autoincrement,
                                 `order_id` smallint(5)  NOT NULL,
                                 `product_id` smallint(5)  NOT NULL,
                                 `quantity` int(10)  NOT NULL DEFAULT 1
) ;

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `quantity`) VALUES
                                                                             (1, 1, 1, 1),
                                                                             (2, 1, 3, 2),
                                                                             (3, 2, 13, 1),
                                                                             (4, 2, 10, 2),
                                                                             (5, 3, 2, 1),
                                                                             (6, 3, 9, 1),
                                                                             (7, 4, 4, 2),
                                                                             (8, 4, 12, 1),
                                                                             (9, 5, 2, 1),
                                                                             (10, 5, 11, 1);
CREATE TABLE `products` (
                            id INTEGER not null primary key autoincrement,
                            `name` varchar(20) NOT NULL,
                            `image_url` varchar(255) NOT NULL,
                            `description` text NOT NULL,
                            `weight` int(10)  NOT NULL,
                            `available` tinyint(4) DEFAULT 1,
                            `price` int(10)  NOT NULL,
                            `quantity` int(10)  DEFAULT NULL
) ;

INSERT INTO `products` (`id`, `name`, `image_url`, `description`, `weight`, `available`, `price`, `quantity`) VALUES
                                                                                                                  (1, 'Sed a lorem', 'https://picsum.photos/536/354', 'Suspendisse ac posuere felis, varius malesuada erat. Integer condimentum venenatis pellentesque. Praesent id interdum tortor, non porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 1000, 1, 10000, 10),
                                                                                                                  (2, 'Smalesuada', 'https://picsum.photos/537/354', 'Pon porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 1000, 1, 10000, 10),
                                                                                                                  (3, 'Varius', 'https://picsum.photos/536/355', 'Sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 500, 1, 1000, 1),
                                                                                                                  (4, 'Pellentesque', 'https://picsum.photos/537/355', 'Porttitor velit. Aliquam accumsan, nibh ut ultrices ornare, nibh sem aliquet lectus, sit amet rutrum velit ante sit amet quam. Morbi a fringilla risus. Nullam fermentum ut eros vel fringilla. Aliquam dictum consequat tincidunt. Maecenas ac dignissim metus.', 500, 1, 1000, 0);

-- ALTER TABLE `orders`
--     ADD CONSTRAINT `uder_id` FOREIGN KEY (`uder_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- ALTER TABLE `order_product`
--     ADD CONSTRAINT `order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
--     ADD CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
