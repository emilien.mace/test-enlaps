<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'image_url', 'description', 'weight', 'available', 'price', 'quantity'];

}
