<?php

namespace App\Http\Controllers;

use App\Models\Product;

class HomeController
{

    function show()
    {
        $products = Product::all();
        return view('pages.homePage', [
            'page'=>'homePage',
            'products' => $products
        ]);
    }
}
