<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Models\Customer;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    /** display all customers
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show()
    {
        $users = Customer::all();
        return view('pages.userPage', ['users' => $users, 'page' => 'user']);
    }
    public function index(Customer $user)
    {
//        var_dump($user);
        return view('pages.userValidPage', ['user' => $user,'page' => 'user detail']);
    }
}
