<!doctype html>
    <html>
    <head>
        @include('includes.head')
    </head>
    <body>
        <header class="p-3 bg-dark text-white">
            @include('includes.header')
        </header>
        <main class="container">
            @yield('content')
        </main>
        <footer class="text-center bg-black text-white p-4">
            @include('includes.footer')
        </footer>
    </body>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/bootstrap/js/bootstrap.bundle.min.js"></script>

</html>
