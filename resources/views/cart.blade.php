@extends('layouts.default')
@section('content')

    <main class="container cart">
        <section>
            <div class="card">
                <div class="card-body">

                    <table class="table table-striped  align-middle">
                        <!--EN-TETE PRODUIT-->
                        <thead>
                        <tr class="table-active">
                            <th colspan="2" scope="row">Produit</th>
                            <td>Prix unitaire</td>
                            <td>Quantité</td>
                            <td colspan="2">Total</td>

                        </tr>
                        </thead>
                        @if( !empty($order) )
                            @php
                                $order_total =0
                            @endphp
                            <tbody>

                            @foreach ($order as $product)

                                <!--DISPLAY PRODUIT-->
                                <tr class="table-active">
                                    <th colspan="2" scope="row" class="text-uppercase">
                                        {{ $product->name }}
                                    </th>
                                    <td>
                                        {{ number_format($product->price / 100, 2) }}€
                                    </td>

                                    <td>
                                        {{$product->quantityOrder}}
                                    </td>
                                    <td>
                                        <p>
                                            {{ number_format($product->price *$product->quantityOrder/ 100, 2) }}€
                                            <!-- TTC-->
                                        </p>
                                    </td>

                                    <td>
                                        <form action="#"
                                              method="post">
                                            {{ csrf_field() }}
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <input type="hidden" name="removeOfPanier"
                                                   value="{{ $product->id }}">
                                            <input type="submit" value="X" class="btn-danger">
                                        </form>
                                    </td>
                                </tr>

                                @php
                                    $order_total += $product->price * $product->quantityOrder
                                @endphp
                            @endforeach
                            <tr>
                                <td colspan="6">Total de la
                                    commande:  {{ number_format($order_total / 100, 2) }}€</td>
                            </tr>

                            </tbody>

                            @if(isset($customer))

                                <form action="{{route('cart.store')}}"
                                      method="post">
                                    {{ csrf_field() }}

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <input type="hidden" name="customer_id"
                                           value="{{$customer->id}}">
                                    <input type="hidden" name="order_total"
                                           value="{{$order_total}}">
                                    @foreach($order as $product)
                                        <input type="hidden" name="products[{{$product->id}}]"
                                               value="{{$product->quantityOrder}}">
                                    @endforeach
                                    <input class="btn-primary btn float-end"
                                           type="submit"
                                           value="VALIDER COMMANDE">
                                </form>
                            @else

                                <a href="#"
                                   class="btn btn-outline-light me-2 bg-dark float-end">
                                    Login
                                </a>
                    @endif
                </div>
            </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <p class="text-center">
                            <strong>
                                Panier Vide
                            </strong>
                        </p>
                    </div>
                </div>

                @endif
                </table>
        </section>
    </main>
@stop
