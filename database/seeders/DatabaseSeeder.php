<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user1 = DB::table('customers')->insertGetId(
            [
                'pseudo' => 'user1',
            ]
        );
        $user2 = DB::table('customers')->insertGetId(
            [
                'pseudo' => 'user2',
            ]
        );

        $product1 = DB::table('products')->insertGetId(
            [
                'name' => 'Tikee 3',
                'image_url' => 'https://static.enlaps.io/media/catalog/product/cache/1a3f11036395e9b00c4573a6a46c85aa/e/n/enlaps-tikee-3-base.png',
                'description'  => 'Ne vous souciez jamais de la batterie ou de la carte mémoire de votre caméra timelapse. Tikee gère son énergie en fonction de la lumière du soleil et transmet ses images à distance pendant plusieurs mois, années.',
                'weight'  => 1500,
                'available'  => 1,
                'price'  => 990,
                'quantity'  => 15,
            ]
        );
        $product2 = DB::table('products')->insertGetId(
            [
                'name' => 'Tikee 3 Pro+',
                'image_url' => 'https://static.enlaps.io/media/catalog/product/cache/1a3f11036395e9b00c4573a6a46c85aa/e/n/enlaps-tikee-3-base.png',
                'description'  => 'Tikee 3 PRO+ avec option panneau solaire externe',
                'weight'  => 1800,
                'available'  => 1,
                'price'  => 1800,
                'quantity'  => 10,
            ]
        );


        // \App\Models\Customer::factory(10)->create();

        // \App\Models\Customer::factory()->create([
        //     'name' => 'Test Customer',
        //     'email' => 'test@example.com',
        // ]);
    }
}
